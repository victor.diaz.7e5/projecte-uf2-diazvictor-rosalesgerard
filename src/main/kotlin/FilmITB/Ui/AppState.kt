package FilmITB.Ui

import FilmITB.Model.User
import kotlinx.serialization.Serializable

@Serializable
data class AppState(var currentUser:User? = null){
}