package FilmITB.Ui

import FilmITB.Model.Film
import FilmITB.Model.FilmItb
import FilmITB.Model.User
import java.util.*

//bucle until exiting (while choice != 0, possibly)

class UserUi(val scanner: Scanner, val filmItb: FilmItb, val appState: AppState){
    fun user() {
        println("Users:\n" +
                "1: Add user\n" +
                "2: Show my user\n" +
                "3: View users\n" +
                "4: Update user\n" +
                "5: Delete user\n" +
                "6: Change User\n" +
                "7: Show statistics\n" +
                "0: Return to main menu")
        println("Que quieres hacer?")
        var choiceNumber = scanner.nextInt()

        while (choiceNumber != -1) {
            when (choiceNumber) {
                1 -> addUser()
                2 -> showMyUser()
                3 -> viewUsers()
                4 -> updateUser()
                5 -> deleteUser()
                6 -> changeUser()
                //7 ->
                0 -> break
            }
            println("Users:\n" +
                    "1: Add user\n" +
                    "2: Show my user\n" +
                    "3: View users\n" +
                    "4: Update user\n" +
                    "5: Delete user\n" +
                    "6: Change User\n" +
                    "0: Return to main menu")
            println("Que quieres hacer?")
            choiceNumber = scanner.nextInt()

        }
    }

    private fun addUser() {
        println("Introduzca el nombre del nuevo usuario: ")
        val userName = scanner.next()
        filmItb.addUser(userName)
    }

    private fun showMyUser() {
        return println("Usuario actual: ${appState.currentUser}")
    }

    private fun viewUsers() {
        println("Usuarios:")
        printUsers(filmItb.viewUsers())
    }

    private fun updateUser() {
        println("Escoge un usuario de la lista y luego escribe el nuevo nombre:")
        printUsers(filmItb.users)
        val userToUpdate = scanner.next()
        val newName = scanner.next()
        filmItb.updateUser(userToUpdate, newName)
    }

    private fun deleteUser() {
        println("Escoge un usuario de la lista a eliminar:")
        printUsers(filmItb.users)
        val userToDelete = scanner.next()
        filmItb.deleteUser(userToDelete)
    }

    private fun changeUser() {
        println("Escoge un usuario de la lista:")
        printUsers(filmItb.users)
        val accountUser = scanner.next()
        val user : User? = filmItb.userByUsername(accountUser)
        if (user != null)
            appState.currentUser = user
    }

    private fun printUsers(users: MutableList<User>) {
        for (user in users)
            println("- ${user.userName}")
    }
}

//may have to !! all appState.CurrentUser(s)