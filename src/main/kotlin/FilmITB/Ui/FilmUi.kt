package FilmITB.Ui


import FilmITB.Model.FilmItb
import FilmITB.Model.Film
import java.util.*

class FilmUi(val scanner: Scanner, val filmItb: FilmItb, val appState: AppState) {
    fun menuFilm() {
        println("Films:\n" +
                "1: Add film\n" +
                "2: Show films\n" +
                "3: Delete films\n" +
                "4: Watch films\n" +
                "5: View watched films\n" +
                "6: Add film to favorites\n" +
                "7: Show favorites\n" +
                "8: Show likes per film\n" +
                "0: Return to main menu")

        println("Que quieres hacer?")
        var choiceNumber = scanner.nextInt()
        scanner.nextLine()
        while (choiceNumber != -1) {
            when (choiceNumber) {
                1 -> addFilm()
                2 -> showFilms()
                3 -> deleteFilms()
                4 -> watchFilms()
                5 -> viewWatchedFilms()
                6 -> addFilmToFavorites()
                7 -> showFavorites()
                8 -> showLikesPerFilm()
                0 -> break
            }
            println("Films:\n" +
                    "1: Add film\n" +
                    "2: Show films\n" +
                    "3: Delete films\n" +
                    "4: Watch films\n" +
                    "5: View watched films\n" +
                    "6: Add film to favorites\n" +
                    "7: Show favorites\n" +
                    "8: Show likes per film\n" +
                    "0: Return to main menu")

            println("Que quieres hacer?")
            choiceNumber = scanner.nextInt()
            scanner.nextLine()
        }

    }

    private fun addFilm() {
        println("Introduzca el nombre de la pelicula:")
        val filmName = scanner.nextLine()
        println("Introduzca el nombre del director de la pelicula:")
        val nameDirector = scanner.nextLine()
        println("Introduzca el nombre del actor principal de la pelicula:")
        val nameActor = scanner.nextLine()
        println("Introduzca el genero de la pelicula:")
        val genereFilm = scanner.nextLine()
        println("Introduzca una duracion de pelicula (minutos):")
        val length = scanner.nextDouble()
        filmItb.addFilm(filmName, nameDirector, nameActor, genereFilm, length)
    }

    private fun showFilms() {
        printFilms(filmItb.films)
    }

    private fun printFilms(films: MutableList<Film>) {
        for (film in films)
            println("-${film.filmName}")
    }

    private fun deleteFilms() {
        printFilms(filmItb.films)
        println("Intoduzca el nombre de la pelicula para eliminarla de la lista:")
        val filmName = scanner.next()
        filmItb.deleteFilm(filmName)
    }

    private fun watchFilms() { //watchFilm.
        printFilms(filmItb.films)
        println("Intoduzca el nombre de la pelicula para verla:")
        val filmName = scanner.next()
        filmItb.watchFilms(filmName, appState.currentUser!!)
    }

    private fun viewWatchedFilms() {
        printFilms(filmItb.viewWatchedFilms(appState.currentUser!!))
    }

    private fun addFilmToFavorites() {
        printFilms(filmItb.films)
        println("Intoduzca el nombre de la pelicula para añadirla a favoritas:")
        val filmName = scanner.next()
        filmItb.filmByNameSum(filmName)
        filmItb.addFilmToFavorites(filmName, appState.currentUser!!)
    }

    private fun showFavorites() {
        printFilms(filmItb.showFavorites(appState.currentUser!!))
    }

    private fun showLikesPerFilm() {
        printFilms(filmItb.films)
        println("Intoduzca el nombre de la pelicula para añadirla a favoritas:")
        val filmNameSearch = scanner.next()
        val filmSearch : Film? = filmItb.filmByName(filmNameSearch)
        println(filmItb.showLikesPerFilm(filmSearch!!))

    }

}

