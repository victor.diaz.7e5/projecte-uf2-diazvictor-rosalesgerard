package FilmITB.Ui

import FilmITB.Model.FilmItb
import FilmITB.Model.FilmItbStorage
import java.util.*

class Ui() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val filmItb = FilmItb()
    val appState = AppState()
    val filmItbStorage = FilmItbStorage()

    fun setCurrentUser(){
        println("Log In: ")
        val userName = scanner.next()
        val user = filmItb.addUser(userName)
        appState.currentUser = user
    }
    private fun filmItbStorageLoad(){
        filmItbStorage.load()
    }

    fun start() {
        filmItbStorageLoad()
        println(
            "Welcome to FilmItb:\n" +
                    "1: User\n" +
                    "2: Films\n" +
                    "3: Search\n" +
                    "0: Exit"
        )

        var choice = scanner.nextInt()
        while (choice != 0){

            when (choice) {
                1 -> user()
                2 -> films()
                3 -> searchUi()
            }

            println(
                "Welcome to FilmItb:\n" +
                        "1: User\n" +
                        "2: Films\n" +
                        "3: Search\n" +
                        "0: Exit"
            )

            choice = scanner.nextInt()
        }
        filmItbStorageSave()
    }

    private fun filmItbStorageSave(){
        filmItbStorage.save(FilmItb())
    }

    private fun films() {
        val film = FilmUi(scanner, filmItb, appState)
        film.menuFilm()
    }

    private fun user() {
        val user = UserUi(scanner, filmItb, appState)
        user.user()
    }
    private fun searchUi(){
        val searchUi = SearchUi(scanner,filmItb, appState)
        searchUi.menuSearch()
    }

}

fun main() {
    val ui = Ui()
    ui.setCurrentUser()
    ui.start()
}