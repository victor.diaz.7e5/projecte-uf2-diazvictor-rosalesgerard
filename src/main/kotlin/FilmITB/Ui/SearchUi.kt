package FilmITB.Ui

import FilmITB.Model.Film
import FilmITB.Model.FilmItb

class SearchUi(val scanner: java.util.Scanner, val filmItb: FilmItb, val appState: AppState){

    fun menuSearch(){
        println("Search methods:\n" +
                "1: By title\n" +
                "2: By director\n" +
                "3: By main actor\n" +
                "4: By genere\n" +
                "5: By length\n" +
                "0: Return to main menu")
        println("Que quieres hacer?")
        var choiceNumber = scanner.nextInt()
        scanner.nextLine()
        while (choiceNumber != -1) {
            when (choiceNumber) {
                1 -> searchTitle()
                2 -> searchDirector()
                3 -> searchActor()
                4 -> searchGenere()
                5 -> searchLength()
                0 -> break
            }
            println("Search methods:\n" +
                    "1: By title\n" +
                    "2: By director\n" +
                    "3: By main actor\n" +
                    "4: By genere\n" +
                    "5: By length\n" +
                    "6: Not watched\n" +
                    "7: Recomended\n" +
                    "0: Return to main menu")
            println("Que quieres hacer?")
            choiceNumber = scanner.nextInt()
            scanner.nextLine()
        }

    }

    private fun searchTitle() {
        println("Introduce el nombre de la pelicula:")
        val title = scanner.nextLine()
        println(filmItb.searchTitle(title))
    }

    private fun searchDirector() {
        println("Introduce el nombre del director de la pelicula:")
        val nameDirector = scanner.nextLine()
        println(filmItb.searchDirector(nameDirector))
    }

    private fun searchActor() {
        println("Introduce el nombre del actor principal:")
        val actor = scanner.nextLine()
        println(filmItb.searchActor(actor))
    }

    private fun searchGenere() {
        println("Introduce el genero a filtrar:")
        val genre = scanner.nextLine()
        println(filmItb.searchGenre(genre))
    }

    private fun searchLength() {
        println("Introduce una duracion (min:sec):")
        println("--Se mostraran las peliculas con igual o menor duracion.")
        val duration = scanner.nextDouble()
            println(filmItb.searchDuration(duration))
    }
}