package FilmITB.Model

import kotlinx.serialization.Serializable

@Serializable
data class User(val userName:String, val favoriteList: MutableList<Film> = mutableListOf(), val watchedList: MutableList<Film> = mutableListOf() ){

}