package FilmITB.Model

import kotlinx.serialization.Serializable


@Serializable
data class Film(val filmName:String, val nameDirector:String?= null, val nameActor:String? = null, val genereFilm: String? = null, val length:Double? = null, var filmFavoritesCount: Int = 0, var watchedOrNot:Int = 0){


}