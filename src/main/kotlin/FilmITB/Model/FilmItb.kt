package FilmITB.Model
import kotlinx.serialization.Serializable

@Serializable
data class FilmItb(val users: MutableList<User> = mutableListOf(), val films : MutableList<Film> = mutableListOf()) {

    /**
     * fun addUser used to add name in users list
     */
    fun addUser(userName: String): User{
        val user = User(userName)
        users.add(user)
        return user
    }

    /**
     * updateUser function used to update the name that is in the list of users
     * */
    fun updateUser(userToUpdate: String, newName: String) {
        val userUpdate = User(userToUpdate)
        for (i in 0..users.lastIndex) {
            if (users[i] == userUpdate) {
                users[i] = User(newName)
            }
        }
    }

    /**
     * deleteUser function used to delete the name that is in the list of users
     * */
    fun deleteUser(userToDelete: String) {
        val userDelete = User(userToDelete)
        for (i in 0..users.lastIndex) {
            if (users[i] == userDelete) {
                users.removeAt(i)
                break
            }
        }
    }
    /**
     * viewUsers function used to view the users list
     * */
    fun viewUsers(): MutableList<User> {
        return users
    }
    /**
     * addFilm function used to add name film in the films list
     * */
    fun addFilm(filmName: String, nameDirector: String, nameActor: String, genereFilm: String, length: Double) {
        val film = Film(filmName, nameDirector, nameActor, genereFilm, length)
        films.add(film)
    }
    /**
     * showFilms function used to show the name films in the films list
     * */
    fun showFilms(): MutableList<Film>{
        return films
    }
    /**
     * addFilmToFavorites function used to add the names film in the films list for favoriteList
     * */
    fun addFilmToFavorites(filmName: String, user: User) {
        val film = Film(filmName)
        user.favoriteList.add(film)
    }
    /**
     * watchFilms function it is used so that the user who enters the name of the movie from the list of movies is added to the list of favorites.*/
    fun watchFilms(filmName: String, user: User) {
        val film = Film(filmName)
        user.watchedList.add(film)
    }
    /**
     * showFavorites function is used to show the name of the movies in the favorites list.
     * */
    fun showFavorites(user: User): MutableList<Film>{
        return user.favoriteList
    }
    /**
     * viewWatchedFilms function is used to show the name movie watched
     * */
    fun viewWatchedFilms(user: User): MutableList<Film>{
        return user.watchedList
    }
    /**
     * */
    fun showLikesPerFilm(film: Film): Int {
        return film.filmFavoritesCount
    }

    fun userByUsername(accountUser: String): User? {
        var searchUser : User? = null
        for (user in users) {
            if (user.userName == accountUser) {
                searchUser = user
            }
        }
        return searchUser
    }

    fun filmByFilmsname(filmName: String): Film? {
        var searchFilm : Film? = null
        for (film in films)
            if (film.filmName == filmName)
                searchFilm = film
        return searchFilm
    }

    fun deleteFilm(filmName: String){
        films.remove(filmByFilmsname(filmName))
    }

    fun filmByName(filmSearch: String): Film? {
        var searchFilm : Film? = null
        for (film in films) {
            if (film.filmName == filmSearch) {
                searchFilm = film
            }
        }

        return searchFilm
    }

    fun filmByNameSum(filmSearch: String) {
        var searchFilm: Film? = null
        for (film in films) {
            if (film.filmName == filmSearch) {
                searchFilm = film
            }
        }
        if (searchFilm != null) {

            searchFilm.filmFavoritesCount++
        }
    }

    fun searchTitle(title: String): Film? {
        var searchFilm : Film? = null
        for (film in films)
            if (film.filmName == title)
                searchFilm = film
        return searchFilm
    }

    fun searchDirector(nameDirector: String): Film? {
        var searchDirector : Film? = null
        for (film in films)
            if (film.nameDirector == nameDirector)
                searchDirector = film
        return searchDirector
    }

    fun searchActor(actor: String): Film? {
        var searchFilm : Film? = null
        for (film in films)
            if (film.nameActor == actor)
                searchFilm = film
        return searchFilm
    }

    fun searchGenre(genre: String): Film? {
        var searchFilm : Film? = null
        for (film in films)
            if (film.genereFilm == genre)
                searchFilm = film
        return searchFilm
    }

    fun searchDuration(length: Double): Film? {
        var searchFilm : Film? = null
        for (film in films)
            if (film.length!! <= length)
                searchFilm = film
        return searchFilm
    }
    fun notWatched() {
        //list
        for (film in films) {
        if (film.watchedOrNot == 0) {
            return
        }
        }
    }

    fun filmWatchedSum(filmSearch: String) {
        var searchFilm: Film? = null
        for (film in films) {
            if (film.filmName == filmSearch) {
                searchFilm = film
            }
        }
        if (searchFilm != null) {

            searchFilm.watchedOrNot++
        }
    }


}
//UTILITIES------

    /*
    fun listSeeker() {
        for (i in 0..users.lastIndex) {
        }

    }

     data class Box(val int: Int)
    fun main(){
        val list = MutableList(5){Box(it)}
        var boxDelete : Box? = null
        for (item in list)
            if (item.int == 2)
                boxDelete = item
    }
    list.remove(boxDelete)
     */
